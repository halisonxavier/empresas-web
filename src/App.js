import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { Route } from "react-router-dom";
import HomePage from "./components/pages/HomePage";
import Login from "./components/pages/Login";
import ListaEmpresas from "./components/pages/ListaEmpresas";
import Empresa from "./components/pages/Empresa";

const App = () => (
  <div>
    <Route path="/" exact component={Login}/>
    <Route path="/home" exact component={HomePage}/>
    <Route path="/listaEmpresas" exact component={ListaEmpresas}/>
    <Route path="/empresa" exact component={Empresa}/>
  </div>
)

export default App;
