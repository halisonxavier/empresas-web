import React from "react";
import { Link } from "react-router-dom"

const ListaEmpresas = () => (
  <div>
    <h1>Empresas</h1>
    <ul>
      <li><Link to="/Empresa">Empresa1</Link></li>
      <li><Link to="/Empresa">Empresa2</Link></li>
    </ul>
  </div>
)

export default ListaEmpresas;
