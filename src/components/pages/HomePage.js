import React from "react";
import { Component } from "react";
import { Link } from "react-router-dom";


class HomePage extends Component {
  constructor() {
    super();
    this.state = {search: false};
  }

  NavBar = () => (
    <nav className="navbar">
    <img src="logo-nav.png" width="100px"></img>
    <span className="fas fa-search" onClick={this.testeHandler}></span>
    </nav>
  );

  SearchBar = () => (
    <nav className="navbar">
    <div className="col-10">
    <input type="search" placeholder="pesquisar" className="form-control"/>
    </div>
    <Link to="/listaEmpresas">
    <button className="btn"><i className="fas fa-search"></i></button>
    </Link>
    </nav>
  );

  testeHandler = () => {
    this.setState({search: true});
  }

  render(){
    if (!this.state.search) {
      return(
        <this.NavBar/>
      );
    } else {
      return(
        <this.SearchBar/>
      );
    }
  }
}

export default HomePage;
